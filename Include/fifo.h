#ifndef __FIFOH__
#define __FIFOH__

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include "mainlib.h"

void *receivFifoThread(void *arg);
void *fireFifoThread(void *arg);
int mkfifoSafe(char *address);

#endif // __FIFOH__
