#ifndef __MAINLIBH__
#define __MAINLIBH__

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>


#include "fifo.h"
#include "tools.h"
#include "udp.h"

#define DOTS_STRING "..........................................."
#define HUL_MSGBUFFER_SIZE 1024

enum ConnType {FIFO, UDP};
enum ConnDirection {SERVER, CLIENT};
enum ConnError{OK = 0};

typedef void (*callbackDefinition)(char *msg, int length, struct CONNHU *conn);

struct CONNHU {
  ConnType ct;
  ConnDirection cd;
  ConnError err;
  char *address;
  char *addressRX; 
  char *addressTX;
  int id;
  FILE fifo;
  int sockFd;
  pthread_t thread;
  callbackDefinition callback;
  char *msgRX;
  int lengthMsgRX;
  char *msgTX;
  int lengthMsgTX;
};

CONNHU *initHu(ConnType ct, ConnDirection cd, char *address, callbackDefinition callback);
int fireMsgHu(CONNHU *conn, char *msg, int size);

#endif // __MAINLIBH__
