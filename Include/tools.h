#ifndef __TOOLSH__
#define __TOOLSH__

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void msleep(int ms);
char * getIpAddress(char *str);
int getIpPort(char *str);
char *addrToString(int ipaddress, int port);

#endif // __TOOLSH__
