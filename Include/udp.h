#ifndef __UDPH__
#define __UDPH__

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "mainlib.h"

void *receivUdpThread(void *arg);
void *fireUdpThread(void *arg);

#endif // __UDPH__
