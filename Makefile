CMPL=g++
INCLUDE=Include
SRC=Src
OBJ=Obj
LIB=Lib
BIN=Bin

LIBDEPEND=$(OBJ)/mainlib.o $(OBJ)/fifo.o $(OBJ)/udp.o $(OBJ)/tools.o


all:libHookUpLib.a example

libHookUpLib.a: $(LIBDEPEND) mkdirlib
		ar rc $(LIB)/libHookUpLib.a $(LIBDEPEND)
		ranlib $(LIB)/libHookUpLib.a


example: $(OBJ)/exampleFifo.o $(OBJ)/exampleUdp.o libHookUpLib.a mkdirbin
		$(CMPL)   $(OBJ)/exampleFifo.o -L./$(LIB) -lHookUpLib -lpthread -o  $(BIN)/exampleFifo
		$(CMPL)   $(OBJ)/exampleUdp.o -L./$(LIB) -lHookUpLib -lpthread -o  $(BIN)/exampleUdp

$(OBJ)/%.o: $(SRC)/%.c mkdirobj
		$(CMPL) -c $< -o $@ -I$(INCLUDE)
	       

mkdirobj:
		mkdir -p $(OBJ)

mkdirlib:
		mkdir -p $(LIB)
				
mkdirbin:
		mkdir -p $(BIN)
		
clean:
		rm -R $(OBJ) $(LIB) $(BIN)
		




