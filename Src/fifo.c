#include "fifo.h"

void *receivFifoThread(void *arg) {
  CONNHU *conn = (CONNHU*) arg;
  callbackDefinition callback = conn->callback;
  FILE *fFifo;
  int receiveDataLength;
  char buffer[HUL_MSGBUFFER_SIZE];
  char *msg;

  printf("FIFO THREAD%sRUN\n",DOTS_STRING);
 
  while (1) {
    if ((fFifo = fopen(conn->addressRX, "r")) == NULL) {
      printf("error can't open pipe \"%s\"\n", conn->addressRX);
      return NULL;
    }
    
    receiveDataLength = fread(buffer, sizeof(char), HUL_MSGBUFFER_SIZE, fFifo);

    msg = (char*) calloc(receiveDataLength, sizeof(char));

    for (int i = 0; i<receiveDataLength; i++) {
      msg[i] = buffer[i];
    }

    (*callback)(msg, receiveDataLength, conn);
    fclose(fFifo);
    free(msg);
  }
  return NULL;
}

void *fireFifoThread(void *arg) {
  CONNHU *conn = (CONNHU*) arg;
  FILE *fFifo;
  
  if ((fFifo = fopen(conn->addressTX, "w")) == NULL) {
    printf("error can't open pipe \"%s\"\n", conn->addressTX);
    return NULL;
  }
  fwrite(conn->msgTX, sizeof(char), conn->lengthMsgTX, fFifo);
  fclose(fFifo);

  return NULL;
}

int mkfifoSafe(char *address) {
  struct stat status;

  if (stat(address, &status) != 0) {
    //unlink(address);
    if (mkfifo(address, 0777) < 0) {
      printf("ERROR\ncan't create pipe\n");
      return -1;
    } else {
      printf("OK\n");
    }
  }

  return 0;
}

