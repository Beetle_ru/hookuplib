#include "udp.h"


void *receivUdpThread(void *arg) {
	CONNHU *conn = (CONNHU*) arg;
	callbackDefinition callback = conn->callback;
	
	int receiveDataLength;
	struct sockaddr_in servaddr, cliaddr;
	socklen_t structLen;
	char buffer[HUL_MSGBUFFER_SIZE];
	char *msg;

	if (conn->cd == SERVER) {
		bzero(&servaddr,sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_addr.s_addr=htonl(inet_addr(getIpAddress(conn->addressRX)));
		servaddr.sin_port=htons(getIpPort(conn->addressRX));
		bind(conn->sockFd,(struct sockaddr *)&servaddr,sizeof(servaddr));
		printf("### %d, %d\n", servaddr.sin_port, htons(servaddr.sin_port));
	}

	while (1) {
		structLen = sizeof(sockaddr_in);
		receiveDataLength = recvfrom(conn->sockFd,buffer,HUL_MSGBUFFER_SIZE,0,(struct sockaddr *)&cliaddr,&structLen);
		msg = (char*) calloc(receiveDataLength, sizeof(char));
		for (int i = 0; i<receiveDataLength; i++) {
			msg[i] = buffer[i];
		}
		free(conn->addressRX);
		conn->addressRX = addrToString(cliaddr.sin_addr.s_addr, htons(cliaddr.sin_port));
		(*callback)(msg, receiveDataLength, conn);
		free(msg);

	}
	return NULL;
}

void *fireUdpThread(void *arg) {
	CONNHU *conn = (CONNHU*) arg;
	struct sockaddr_in servaddr;
	char *ipAddress = getIpAddress(conn->addressTX);

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr=inet_addr(ipAddress);
	servaddr.sin_port=htons(getIpPort(conn->addressTX));

	sendto(conn->sockFd, conn->msgTX, conn->lengthMsgTX, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));

	free(ipAddress);

	return NULL;
}
