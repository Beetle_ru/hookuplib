#include "mainlib.h"

CONNHU *initHu(ConnType ct, ConnDirection cd, char *address, callbackDefinition callback) {
	CONNHU *conn = (CONNHU*) malloc(sizeof(CONNHU));

	char *addressRX;// = (char*) malloc(strlen(address)+3);
	char *addressTX;// = (char*) malloc(strlen(address)+3);


	conn->callback = callback;
	conn->address = address;
	conn->ct = ct;
	conn->cd = cd;
	conn->id = 1133;

	switch (ct) {
		case FIFO:
			addressRX = (char*) malloc(strlen(address)+3);
			addressTX = (char*) malloc(strlen(address)+3);
			strcpy(addressRX, address); 
			strcpy(addressTX, address);

			printf("FIFO%sOK\n", DOTS_STRING);
			if (cd == SERVER) {
				printf("SERVER%s", DOTS_STRING);
				strcat(addressRX, ".RS");
				strcat(addressTX, ".TS");

				if (mkfifoSafe(addressRX) !=0)
					return NULL;
				if (mkfifoSafe(addressTX) !=0)
					return NULL;
			} else {
				strcat(addressRX, ".TS");
				strcat(addressTX, ".RS");
			}

			conn->addressRX = addressRX;
			conn->addressTX = addressTX;

			printf("THREAD CREATE%s", DOTS_STRING);    
			if ( pthread_create(&conn->thread, NULL, receivFifoThread, conn) != 0) {
				printf("ERROR\n");
			} else {
				printf("OK\n");
			}

		break;
    
		case UDP:
			conn->sockFd = socket(AF_INET,SOCK_DGRAM,0);
			if (cd == SERVER) {
				conn->addressRX = (char*) malloc(strlen(address));
				strcpy(conn->addressRX, address);
			} else {
				char * a = "0.0.0.0:0";
				conn->addressRX = (char*) malloc(strlen(a));
				strcpy(conn->addressRX, a);
				conn->addressTX = (char*) malloc(strlen(address));
				strcpy(conn->addressTX, address); 
			}

			printf("THREAD CREATE%s", DOTS_STRING);    
			if ( pthread_create(&conn->thread, NULL, receivUdpThread, conn) != 0) {
				printf("ERROR\n");
			} else {
				printf("OK\n");
			}
		break;

		default:
			printf("unsupported type connection\n");
			return NULL;
		break;
	}
	return conn;
}

int fireMsgHu(CONNHU *conn, char *msg, int size) {
	pthread_t thread;
	conn->msgTX = msg;
	conn->lengthMsgTX = size;
	switch (conn->ct) {
		case FIFO:
			if ( pthread_create(&thread, NULL, fireFifoThread, conn) != 0) {
				printf("ERROR send tread dont't created\n");
				return -1;
			}
		break;
		
		case UDP:
			if ( pthread_create(&thread, NULL, fireUdpThread, conn) != 0) {
				printf("ERROR send tread dont't created\n");
				return -1;
			}
		break;

		default:
			printf("unsupported type connection\n");
			return NULL;
		break;
	}

	pthread_detach(thread);
	return 0;
}
