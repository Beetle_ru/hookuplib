#include <stdio.h>
#include "mainlib.h"

void receiverSrv(char *msg, int length, struct CONNHU *conn);
void receiverCli(char *msg, int length, struct CONNHU *conn);

struct CONNHU *connSrv;
struct CONNHU *connCli;

int main()
{
	connSrv = initHu(UDP, SERVER, (char *)"0.0.0.0:32000", receiverSrv);
	connCli = initHu(UDP, CLIENT, (char *)"127.0.0.1:32000", receiverCli);
	
	//printf("a>>> %s\n", getIpAddress((char *)"127.0.0.1:3000"));
	//printf("p>>> %d\n", getIpPort((char *)"127.0.0.1:3000"));
	
	msleep(1000);
	for(int i = 0; i < 15; i++) {
		char buffer[200];
		int sizemsg = sprintf(buffer, "client request index = %d", i);
		fireMsgHu(connCli, buffer, sizemsg);
		msleep(500);
	}
	printf("press \"Enter\" for exit\n");
	getchar();

	return 0;
}

void receiverSrv(char *msg, int length, struct CONNHU *conn) {
	char buffer[200];
	int sizemsg;

	printf("srv: msg from %s >> \"%s\", length = %d\n", conn->addressRX, msg, length);
	sizemsg = sprintf(buffer, "OK");
	free(conn->addressTX);
	conn->addressTX = (char *)malloc(sizeof(char) * strlen(conn->addressRX)); 
	strcpy(conn->addressTX, conn->addressRX);
	fireMsgHu(conn, buffer, sizemsg);
}

void receiverCli(char *msg, int length, struct CONNHU *conn) {
	printf("cli: msg from %s >> \"%s\", length = %d\n", conn->addressRX, msg, length);
}
