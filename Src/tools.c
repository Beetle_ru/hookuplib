#include "tools.h"

void msleep(int ms) {
  usleep(ms * 1000);
  return;
}

char *getIpAddress(char *str) {
	char *p = strchr(str, ':');
	if (p == NULL) return str;
	char *address = (char*) calloc(p - str, sizeof(char));
	strncpy(address, str, p - str);
	return address;
}

int getIpPort(char *str) {
	char *p = strchr(str, ':');
	if (p == NULL) return 0;
	return atoi(++p);
}

char *addrToString(int ipaddress, int port) {
	char * str = (char*) calloc(22, sizeof(char));
	unsigned char *c = (unsigned char *) &ipaddress;
	sprintf(str, "%d.%d.%d.%d:%d", c[0], c[1], c[2], c[3], port);
	realloc(str, sizeof(char) * (strlen(str)));
	return str;
} 
