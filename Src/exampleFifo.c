#include <stdio.h>
#include "mainlib.h"

void receiverSrv(char *msg, int length, struct CONNHU *conn);
void receiverCli(char *msg, int length, struct CONNHU *conn);

struct CONNHU *connSrv;
struct CONNHU *connCli;

int main()
{
  connSrv = initHu(FIFO, SERVER, (char *)"FifoChannel", receiverSrv);
  connCli = initHu(FIFO, CLIENT, (char *)"FifoChannel", receiverCli);
	
  msleep(1000);
  for(int i = 0; i < 15; i++) {
    char buffer[200];
    int sizemsg = sprintf(buffer, "client request index = %d", i);
    fireMsgHu(connCli, buffer, sizemsg);
    msleep(500);
  }
  printf("press \"Enter\" for exit\n");
  getchar();
 
  return 0;
}

void receiverSrv(char *msg, int length, struct CONNHU *conn) {
  char buffer[200];
  int sizemsg;
  	
  printf("msg from %s >> \"%s\", length = %d\n", conn->addressRX, msg, length);
  	
  sizemsg = sprintf(buffer, "OK");
  fireMsgHu(connSrv, buffer, sizemsg);
}

void receiverCli(char *msg, int length, struct CONNHU *conn) {
  printf("msg from %s >> \"%s\", length = %d\n\n", conn->addressRX, msg, length);
}
